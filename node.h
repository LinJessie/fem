#ifndef NODE_H
#define NODE_H


#include"femobject.h"


 class Node
 {

 public:

     Node(){}
     ~Node(){}

     Node(GMlib::TSVertex<float> &vert){
        _vertex=&vert;

     }

     GMlib::TSVertex<float>* getVertex(){
         return _vertex;
     }

     GMlib::Array<GMlib::TSEdge<float> *> getEdges(){//
         return _vertex->getEdges();
     }//

     GMlib::Array<GMlib::TSTriangle<float>* > getTriangles(){//
         return _vertex->getTriangles();
     }//

// protected:
// void localSimulate(double dt) {

//     rotate( GMlib::Angle(90) * dt, GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ) );
//     rotate( GMlib::Angle(180) * dt, GMlib::Vector<float,3>( 1.0f, 1.0f, 0.0f ) );
//   }


 private:

    GMlib::TSVertex<float>*  _vertex;


   //


 };







#endif
